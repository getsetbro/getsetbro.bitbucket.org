﻿(function () {
    "use strict";
    var $documentbody = $(document.body),
        driverTemplate = { AvgLapSpeed: null, BestLap: 1, BestLapAvgSpeed: "0", BestLapTime: "0", CarNumber: "0",
        Class: "IndyCar", CompetitorId: "0", CompletedLaps: 1, CurrentLap: 1, Diff: "0.0000", DriverId: "0",
        DriverName: "Blank", EndPlateLarge: "/Content/assets/blank.png", EngineVendorName: "Chevy",
        EntrantId: null, Equipment: "D/C/C/F", Flag: "/Content/assets/blank.png", Gap: "0.0000",
        InPitStop: true, IsHot: false, LapsLed: "0", LastLapAvgSpeed: "0", LastLapTime: "0", License: "Rookie",
        OptimalLapTime: "0", OverallPointsBehind: 0, OverallPosition: 0, Points: 0, Poles: "0", Position: 0,
        SectionBests: 0, StartPosition: 0, TeamId: "9", TireType: "P", Wins: "0" },
        //jsonUrl = "https://imsdigexpstoragenc.blob.core.windows.net/racedata/WebAppDataModel.json";
        jsonUrl = "json.json";

    function getRaceMetaData() {
        return $.ajax(
            {
                url: jsonUrl,
                cache: false,
                dataType: 'json'
            });
    }

    function AppViewModel() {

        var self = this;

        self.contentLoaded = ko.observable(true);

        //expand / collapse sections
        self.tablelistTab = ko.observable("live");
        self.tablelistLive = function () { self.tablelistTab("live"); };
        self.tablelistHot = function () { self.tablelistTab("hot"); };

        self.sectionMode = ko.observable("less");
        self.sectionModeToLess = function () {
            self.tablelistMode("less");
            self.sectionMode("less");
        };
        self.sectionModeToMore = function () {
            self.tablelistMode("less");
            self.sectionMode("more");
        };
        self.driversMode = ko.observable("less");
        self.driversModeToLess = function () {
            self.tablelistMode("less");
            self.driversMode('less');
        };
        self.driversModeToMore = function () {
            self.tablelistMode("less");
            self.driversMode("more");
        };
        self.bothModeToLess = function () {
            self.driversModeToLess();
            self.sectionMode("less");
        };
        self.tablelistMode = ko.observable("less");
        self.tablelistHide = function () {
            self.bothModeToLess();
        };
        self.tablelistShow = function () {
            self.bothModeToLess();
            self.tablelistMode("more");
        };

        self.trackSrc = function (trackFilename) {
            if (trackFilename !== null) {
                return '/Content/assets/track/' + trackFilename.toLowerCase();
            }
            return "/Content/assets/blank.png";
        };
        self.engSrc = function (eng) {
            if (eng !== null) {
                return '/Content/assets/' + eng.toLowerCase() + '-logo-icon.png';
            }
            return "/Content/assets/blank.png";
        };
        var tireObj = { P: 'black', O: 'red', R: 'gray', A: 'black' };
        self.tireSrc = function (tire) {
            return '/Content/assets/Firestone-Tire-logo-' + tireObj[tire] + '.png';
        };
        self.imgSrc = function (str) {
            var name = str !== null ? str.split(' ').join('') : "Blank_headshot";
            return '/Content/assets/driver/' + name + '.png';
        };
        self.imgTallSrc = function (str) {
            var name = str !== null ? str.split(' ').join('') : "Blank";
            return '/Content/assets/driver/' + name + '-profile.png';
        };
        var flagObj = {
            "Green": "green",
            "Yellow": "yellow",
            "Red": "red",
            "White": "white",
            "Checkered": "checkered",
            "Unflagged (warmup)": "gray",
            "Cold (between sessions)": "gray"
        };
        self.flagImg = function (str) {
            var flag = str !== null ? flagObj[str] : "black";
            if (flag) {
                return "/Content/assets/flag-" + flag + ".png";
            }
            return "/Content/assets/blank.png";
        };

        self.getEngine = function (carNum) {
            var driver = _.find(self.driverInfoList(), { 'CarNumber': carNum });
            if (typeof driver !== "undefined") {
                return self.engSrc(driver.EngineVendorName);
            }
            return '/Content/assets/blank.png';
        };
        self.getHead = function (carNum) {
            var driver = _.find(self.driverInfoList(), { 'CarNumber': carNum });
            if (typeof driver !== "undefined") {
                return self.imgSrc(driver.DriverName);
            }
            return '/Content/assets/driver/Blank_headshot.png';
        };
        self.getPlate = function (carNum) {
            var driver = _.find(self.driverInfoList(), { 'CarNumber': carNum });
            if (typeof driver !== "undefined") {
                return driver.EndPlateLarge;
            }
            return '/Content/assets/blank.png';
        };
        self.getName = function (carNum) {
            var driver = _.find(self.driverInfoList(), { 'CarNumber': carNum });
            if (typeof driver !== "undefined") {
                return driver.DriverName;
            }
            return 'Blank';
        };
        self.fix3 = function (num) {
            num = num - 0;
            if (!isNaN(num)) {
                return num.toFixed(3);
            }
            return '-';
        };
        self.formatSpeed = function (speed) {
            return speed.toFixed(3);
        };
        self.formatTime = function (str) {
            if (typeof str === "string") {
                var input = str;
                var strout = "";
                var strarr = str.split(':');
                if (typeof strarr !== "undefined") {
                    if (strarr.length === 3) { // hh:mm:ss.fffffff
                        strout = strarr[1] + ":" + strarr[2];
                        strout = strout.slice(0, -3); //00:00:02.9764534 ==> 00:02.9764
                    }
                    else if (strarr.length === 2) { // mm:ss.fffff
                        strout = strarr[0] + ":" + strarr[1];
                        strout = strout.slice(0, -1); //02:15.97640 ==> 02:15.9764
                    }
                    else if (strarr.length === 1) {
                        strout = strarr[0];
                    }
                }
                return strout;
            }
            return '-';
        };
        self.getRaceDescription = function (eventName, runName, eventRound) {
            if (runName === 'Race') {
                return eventName;
            }
            else {
                return eventName + ' (' + runName + ')';
            }
        };
        self.numCheck = function (num) {
            if (isNaN(num)) {
                return '-';
            }
            return num;
        };
        self.makeStatus = function (isHot, inPitStop, comment) {
            if (comment && comment.length > 0) {
                return 'out';
            }
            if (isHot) {
                return 'hot';
            }
            if (inPitStop) {
                return 'pit';
            }
            return 'nostatus';
        };

        self.lapsDD = ko.observable("All Laps");
        self.towDD = ko.observable("Tow-Yes");
        self.tiresDD = ko.observable("Any Tire");

        var ddFunctions = {
            lapsAll: function () { self.lapsDD("All Laps"); },
            lapsLast5: function () { self.lapsDD("Last 5 Laps"); },
            lapsLast10: function () { self.lapsDD("Last 10 Laps"); },
            towYes: function () { self.towDD("Tow-Yes"); },
            towNo: function () { self.towDD("Tow-No"); },
            tireAny: function () { self.tiresDD("Any Tire"); },
            tirePrimary: function () { self.tiresDD("Primary Tire"); },
            tireOptional: function () { self.tiresDD("Optional Tire"); },
            tireRain: function () { self.tiresDD("Rain Tire"); }
        };
        self.ddChoice = function (appVM, e) {
            ddFunctions[e.target.dataset.opt]();
        };

        self.popoverIndex = ko.observable(0);
        self.hidePopovers = function () {
            $('.popover').slideUp();
            $('#section_info').attr('data-active', 'none');
        };
        self.popOver = function (data, e) {
            var $el = $(e.target);

            if ($el.hasClass('haspopover')) {
                var thisection = e.target.dataset.section;
                var sectionPlus = parseInt(thisection) + 1;
                var offsetTop = $el.offset().top + 50;
                $('#section_info').attr('data-active', 'section' + thisection);
                self.popoverIndex(thisection);
                $('.popover').css('top', offsetTop + 'px').slideDown();
            } else {
                self.hidePopovers();
            }
        };

        self.runInfo = ko.observable();
        self.trackInfo = ko.observable();
        self.lapInfo = ko.observable();
        self.weather = ko.observable();

        // top drivers and speeds
        self.fastestFiveSpeed = ko.observable("0");
        self.fastestFiveTime = ko.observable("0");
        self.fastestLastDriver = ko.observable();

        self.lastLapTime = ko.observable("0");
        self.lastLapTopSpeedValue = ko.observable("0");
        self.lastLapTopSpeedDriver = ko.observable();

        self.bestLapTime = ko.observable("0");
        self.bestLapTopSpeed = ko.observable("0");
        self.bestLapTopSpeedLapNumber = ko.observable("0");
        self.bestLapTopSpeedDriver = ko.observable();

        self.leader = ko.observable(driverTemplate);
        self.leaderSections = ko.observableArray();

        self.driverInfoList = ko.observableArray();
        self.driverHotList = ko.observableArray();

        //sectionlist
        self.bestSectionsList = ko.observableArray();

        //build it all on load
        function buildVM(raceDM) {
            if (typeof raceDM !== "undefined") {
                self.runInfo(raceDM.RunInfo);
                try {
                    self.trackInfo(raceDM.TrackInfo);
                }
                catch (exception) {
                    console.log("failed to set trackInfo in buildVM");
                    console.log(exception);
                }

                var raceFocus = "time";
                if (raceDM.TrackInfo.TrackType === "I" || raceDM.TrackInfo.TrackType === "O") {
                    raceFocus = "speed";
                }

                self.lapInfo(raceDM.LapInfo);
                self.weather(raceDM.WeatherInfo);
                self.bestSectionsList(raceDM.BestSectionsList);

                var list = _.sortBy(raceDM.DriverInfoList, "Position");
                if (typeof list !== "undefined") {
                    self.driverInfoList(list);
                    if (typeof raceDM !== undefined && list.length > 0) {
                        self.leader(list[0]);
                        var leaderSections = _.find(raceDM.DriverSectionInfoList, { "CarNumber": list[0].CarNumber });
                        if (typeof leaderSections !== "undefined") {
                            var arr;
                            if (raceFocus === "speed") {
                                arr = _.map(leaderSections.LastLapSectionInfo, function (value, key) {
                                    return value.SectionSpeed;
                                });
                            }
                            else {
                                arr = _.map(leaderSections.LastLapSectionInfo, function (value, key) {
                                    return self.formatTime(value.SectionTime);
                                });
                            }
                            self.leaderSections(arr);
                        }
                    }
                }

                if (typeof raceDM !== undefined) {
                    var hotlist = _.orderBy(raceDM.DriverInfoList, ["HotPoints", "Position"], ['desc', 'asc']);
                }
                if (typeof hotlist !== "undefined") {
                    self.driverHotList(hotlist);
                }

                if (typeof raceDM !== undefined) {
                    // top drivers and speeds
                    var y = raceDM.BestAndFastestInfo.FastestLastFiveLapsDriver;
                    var x = raceDM.BestAndFastestInfo.FastestLastFiveLapsAvgSpeed;
                    self.fastestFiveSpeed(raceDM.BestAndFastestInfo.FastestLastFiveLapsAvgSpeed);
                    self.fastestFiveTime(raceDM.BestAndFastestInfo.FastestLastFiveLapsTime);
                    self.fastestLastDriver(raceDM.BestAndFastestInfo.FastestLastFiveLapsDriver);

                    self.lastLapTopSpeedValue(raceDM.BestAndFastestInfo.LastLapTopSpeed);
                    self.lastLapTime(raceDM.BestAndFastestInfo.LastLapTime);
                    self.lastLapTopSpeedDriver(raceDM.BestAndFastestInfo.LastLapTopSpeedDriver);

                    self.bestLapTopSpeed(raceDM.BestAndFastestInfo.BestLapTopSpeed);
                    self.bestLapTime(raceDM.BestAndFastestInfo.BestLapTime);
                    self.bestLapTopSpeedLapNumber(raceDM.BestAndFastestInfo.BestLapTopSpeedLapNumber);
                    self.bestLapTopSpeedDriver(raceDM.BestAndFastestInfo.BestLapTopSpeedDriver);
                }
            }
        }

        self.fastestFive = ko.computed(function () {
            var driver = _.find(self.driverInfoList(), { "CarNumber": self.fastestLastDriver() });
            if (typeof driver !== "undefined") {
                return driver;
            }
            return driverTemplate;
        });
        self.lastLapTopSpeed = ko.computed(function () {
            var driver = _.find(self.driverInfoList(), { "CarNumber": self.lastLapTopSpeedDriver() });
            if (typeof driver !== "undefined") {
                return driver;
            }
            return driverTemplate;
        });
        self.bestLap = ko.computed(function () {
            var driver = _.find(self.driverInfoList(), { "CarNumber": self.bestLapTopSpeedDriver() });
            if (typeof driver !== "undefined") {
                return driver;
            }
            return driverTemplate;
        });

        // driver in modal
        self.modalCarNumber = ko.observable(0);
        self.modalSections = ko.observableArray();

        self.updateModalDriver = function (carNum) {
            if (typeof carNum !== undefined) {
                self.modalCarNumber(carNum);
                var modalSections = _.find(raceDataModel.DriverSectionInfoList, { "CarNumber": carNum });
                if (typeof modalSections !== "undefined") {
                    self.modalSections(modalSections);
                }
            }
        };

        self.modalDriver = ko.computed(function () {
            var driver = _.find(self.driverInfoList(), { 'CarNumber': self.modalCarNumber() });
            if (typeof driver !== "undefined") {
                return driver;
            }
            return driverTemplate;
        });

        self.modalLastLapSpeed = ko.computed(function () {
            var arr = [];
            if (typeof self.modalSections().LastLapSectionInfo !== "undefined") {
                arr = _.map(self.modalSections().LastLapSectionInfo, function (value) {
                    return self.fix3(value.SectionSpeed);
                });
            }
            return arr;
        });

        self.modalBestLapSpeed = ko.computed(function () {
            var arr = [];
            if (typeof self.modalSections().BestLapSectionInfo !== "undefined") {
                arr = _.map(self.modalSections().BestLapSectionInfo, function (value) {
                    return self.fix3(value.SectionSpeed);
                });
            }
            return arr;
        });

        self.modalLastLapTime = ko.computed(function () {
            var arr = [];
            if (typeof self.modalSections().LastLapSectionInfo !== "undefined") {
                arr = _.map(self.modalSections().LastLapSectionInfo, function (value) {
                    return self.formatTime(value.SectionTime);
                });
            }
            return arr;
        });

        self.modalBestLapTime = ko.computed(function () {
            var arr = [];
            if (typeof self.modalSections().BestLapSectionInfo !== "undefined") {
                arr = _.map(self.modalSections().BestLapSectionInfo, function (value) {
                    return self.formatTime(value.SectionTime);
                });
            }
            return arr;
        });

        self.sectionInfoListLast5 = ko.computed(function () {
            var list = [];
            ko.utils.arrayForEach(self.bestSectionsList(), function (items) {
                var obj = {
                    SectionIndex: items.SectionIndex,
                    BestSectionDetailsList: []
                };
                _.forEach(items.BestSectionDetailsList, function (v) {
                    if (v.Lap + 5 > self.lapInfo().CurrentLap) {
                        obj.BestSectionDetailsList.push(v);
                    }
                });
                list.push(obj);
            });
            return list;
        });
        self.sectionInfoListLast10 = ko.computed(function () {
            var list = [];
            ko.utils.arrayForEach(self.bestSectionsList(), function (items) {
                var obj = {
                    SectionIndex: items.SectionIndex,
                    BestSectionDetailsList: []
                };
                _.forEach(items.BestSectionDetailsList, function (v) {
                    if (v.Lap + 10 > self.lapInfo().CurrentLap) {
                        obj.BestSectionDetailsList.push(v);
                    }
                });
                list.push(obj);
            });
            return list;
        });

        self.sectionInfoList = ko.computed(function () {
            if (self.lapsDD() === "Last 5 Laps") {
                return self.sectionInfoListLast5();
            }
            if (self.lapsDD() === "Last 10 Laps") {
                return self.sectionInfoListLast10();
            }
            return self.bestSectionsList();
        });

        self.popoverData = ko.computed(function () {
            return self.sectionInfoList()[self.popoverIndex()];
        });

        self.isRace = ko.computed(function () {
            return self.runInfo() && self.runInfo().RunType === 'Race';
        });

        self.isPractice = ko.computed(function () {
            return self.runInfo() && self.runInfo().RunType === 'Practice';
        });

        self.isQualifying = ko.computed(function () {
            return self.runInfo() && self.runInfo().RunType === 'Qualify';
        });

        self.isIndy500 = ko.computed(function () {
            return self.runInfo() && self.runInfo().EventName.indexOf('Indianapolis 500') !== -1;
        });

        self.isIndyCar = ko.computed(function () {
            return self.runInfo() && self.runInfo().SessionSeries === 'I';
        });


        self.getIthLapSpeed = function (lapArray, lapNumber) {
            for (var i = 0; i < lapArray.length; i++) {
                var lap = lapArray[i];

                if (lap.Item1 === lapNumber) {
                    return lap.Item2;
                }
            }

            return 0;
        };

        self.getTireType = function (tireType) {
            switch (tireType) {
                case 'P':
                    return 'Primary';
                case 'A':
                    return 'Alternate';
                case 'W':
                    return "Weather";
            }

            return tireType;
        };

        self.getLogo = function () {
            if (self.runInfo() && self.runInfo().SessionSeries) {
                switch (self.runInfo().SessionSeries) {
                    case 'I': //Indy Car
                        return 'https://www.imscdn.com/TSImages/2016/vics_logo.png';

                    case 'P': //Indy Lights
                        return 'https://www.imscdn.com/TSImages/2016/il_logo.png';

                    case 'S': //Pro Mazda
                        return 'https://www.imscdn.com/TSImages/2016/pm_logo.png';

                    case 'F': //USF2000
                        return 'https://www.imscdn.com/TSImages/2016/usf2000_logo.png';
                }
            }
            else {
                return 'https://www.imscdn.com/TSImages/2016/vics_logo.png';
            }
        };

        self.getLogoTitle = function () {
            if (self.runInfo() && self.runInfo().SessionSeries) {
                switch (self.runInfo().SessionSeries) {
                    case 'I': //Indy Car
                        return 'Verizon IndyCar Series';

                    case 'P': //Indy Lights
                        return 'Indy Lights Presented by Cooper Tires';

                    case 'S': //Pro Mazda
                        return 'Pro Mazda Championship Presented by Cooper Tires';

                    case 'F': //USF2000
                        return 'Cooper Tires USF2000 Championship Powered by Mazda';
                }
            }
            else {
                return 'Verizon IndyCar Series';
            }
        };

        //get data. build.

        if (typeof raceDataModel !== "undefined") {
            buildVM(raceDataModel);
        }

        window.recursivePolling = true;

        function recurs() {
            if (recursivePolling) {
                setTimeout(poll, 4000);
            }
        }

        function poll() {
            var jqxhr = getRaceMetaData()
              .done(function (data) {
                  try {
                      raceDataModel = data;
                      buildVM(raceDataModel);
                  }
                  catch (exception) {
                      console.log(exception.message);
                  }
              })
              .fail(function (jqxhr, textStatus, error) {
                  console.log(textStatus + " " + error);
              })
              .always(function (data) {
                  //recurs();
              });
        }

        recurs();
    }

    //Load data directyle from Azure and apply KnockOut bindings after load
    if (typeof raceDataModel === 'undefined') {
        getRaceMetaData()
            .done(function (data) {
                window.raceDataModel = data;
                window.koModel = new AppViewModel();
                ko.applyBindings(koModel);
            });
    }
}());